<?php
include_once 'fonction.php';

$mdpHashe = GetMdp($_POST['login']); //On utilise la fonction et stock le mdp dans la variable $mdpHashe  
$hash_mdp =md5 ($_POST['password']);

if(!isset($_POST['login']) && !isset($_POST['password'])) { //Si l'utilisateur a laissé les champs vide il retourne à la page de connexion 
    header('Location: connexion.php');
}
else{ //Sinon on vérifie que le mdp saisi est identique a celui hashé de la bdd
    if ($hash_mdp == $mdpHashe ) {
        setcookie('ArmadaLogin', $_POST['login'], time() + (86400 * 30), null, null, false, true);
        header('Location: index.php');//si oui il arrive sur la page d'accueil connecté
    } else {
        header('Location: inscription.php');//sinon il arrive à la page d'inscription, pour éventuellement ce créer un compte
    }
}
?>  