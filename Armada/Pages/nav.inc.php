<nav class="navbar navbar-expand-lg navbar-dark primary-color sticky-top nav-collapse " id="navbar">
  <a class="navbar-brand" href="./index.php">ARMADA 2019</a>
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a data-toggle="collapse" data-target=".navbar-collapse.in" class="nav-link" href="./index.php"><p>Accueil</p>
        </a>
      </li>
      <li class="nav-item">
        <a data-toggle="collapse" data-target=".navbar-collapse" class="nav-link" href="./programme.php"><p>Programme</p></a>
      </li>
      <li class="nav-item">
        <a data-toggle="collapse" data-target=".navbar-collapse" class="nav-link" href="./info_pratique.php"><p>Info pratique</p></a>
      </li>
      <li class="nav-item">
        <a data-toggle="collapse" data-target=".navbar-collapse" class="nav-link" href="./voir_tous_les_bateaux.php"><p>Les Bateaux</p></a>
      </li>
      <li class="nav-item">
        <a data-toggle="collapse" data-target=".navbar-collapse" class="nav-link" href="./sponsors.php"><p>Sponsors</p></a>
      </li>
      <li class="nav-item">
        <a data-toggle="collapse" data-target=".navbar-collapse" class="nav-link" href="./invites.php"><p>Invites</p></a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false"><p>Connexion</p></a>
        <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="./inscription.php"><p>S'inscrire</p></a>
          <a class="dropdown-item" href="./connexion.php"><p>Se connecter</p></a>
        </div>
      </li>
    </ul>
</nav>
<div style="height:25px;display:block;"> </div>





