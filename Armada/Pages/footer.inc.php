<div style="height:50px;display:block;"> </div><!--ajout d'une marge entre le reste de la page et le footer -->
<footer class="page-footer font-small special-color-dark pt-4">
<div class="container">
  <ul class="list-unstyled list-inline text-center">
    <li class="list-inline-item">
    <a class="btn-floating btn-lg btn-fb" href="https://www.facebook.com/ArmadaRouen2019/" ><i class="fab fa-facebook-f"></i></a><!--ajout de l'icone fb et du lien qui redirige sur la page fb de l'armada-->
    </li>
    <li class="list-inline-item">
      <a class="btn-floating btn-tw mx-1" href="https://twitter.com/ArmadaRouen?lang=fr"><i class="fab fa-twitter"></i></a>
    </li>
  </ul>
</div>
<div class="footer-copyright text-center py-3"><p>© 2018 Copyright - Jérémy Lesauvage - Amira Bakkali - Tous droits réservés</p>
</div>
</footer>
</body>
</html>


