<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <title><?php echo($titre_page);?></title>
    <link rel="shortcut icon" href="https://upload.wikimedia.org/wikipedia/commons/d/d8/Vignette_Armada_Rouen_2019.png" type="image/x-icon" />

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/mdb.min.css" rel="stylesheet">
    <link href="stylesheet.css" rel="stylesheet">

    <!-- Metadonnee -->
    <meta name="description" content="Projet scolaire pour l'ESIGELEC année 2018-2019 pour le département TIC cours mo conception de site web">
    <meta name="keywords" content="ESIGELEC, PROJET, ARMADA,2018,2019,SCOLAIRE,CONCEPTION DE SITE WEB, PHP, HTML,CSS">
    <meta name="robots" content="index, nofollow">
    <meta name="web_author" content="LESAUVAGE Jérémy,BAKKALI Amira">
    <meta name="language" content="French">

    <!-- Open graph -->
    <meta property="og:title" content="Armada">
    <meta property="og:site_name" content="Armada">
    <meta property="og:url" content="">
    <meta property="og:description" content="Projet scolaire pour l'ESIGELEC année 2018-2019 pour le département TIC cours mo conception de site web">
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://upload.wikimedia.org/wikipedia/commons/d/d8/Vignette_Armada_Rouen_2019.png">
</head>
