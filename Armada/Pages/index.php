<!-- INCLUDE -->
<?php
$titre_page="Accueil";
include 'header.inc.php';
include 'head.inc.php';
?>

<!-- Carousel d'image de bateau -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators"  style="width:70%;">
    <li data-target="#myCarousel1" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel2" data-slide-to="1"></li>
    <li data-target="#myCarousel3" data-slide-to="2"></li>
    <li data-target="#myCarousel4" data-slide-to="3"></li>
    <li data-target="#myCarousel5" data-slide-to="4"></li>
    <li data-target="#myCarousel6" data-slide-to="5"></li>
    <li data-target="#myCarousel7" data-slide-to="6"></li>
    <li data-target="#myCarousel8" data-slide-to="7"></li>
  </ol>
  <div class="carousel-inner">
    <div class="item active">
      <img src="../images/Bateaux/Dr_YN4HWkAAXu2p.jpg" alt="Bateau 1"  id="myCarouseli1">
      <div class="carousel-caption">
          <h3>Bateau 1</h3>
          <p>Description</p>
      </div>
    </div>
    <div class="item">
      <img src="../images/Bateaux/DniVLB0W4AAaeMF.jpg" alt="Bateau 2"  id="myCarouseli2">
      <div class="carousel-caption">
          <h3>Bateau 2</h3>
          <p>Descritpion</p>
        </div>
    </div>
    <div class="item">
      <img src="../images/Bateaux/DrfrERzWwAIoyhr.jpg" alt="Bateau 3"  id="myCarouseli3">
      <div class="carousel-caption">
          <h3>Bateau 3</h3>
          <p>Description</p>
        </div>
    </div>
    <div class="item">
      <img src="../images/Bateaux/DpNnlzwX4AM6cfh.jpg" alt="Bateau 4" id="myCarouseli4">
      <div class="carousel-caption">
          <h3>Bateau 4</h3>
          <p>Description</p>
        </div>
    </div>
    <div class="item">
      <img src="../images/Bateaux/DogttqZW0AA3hPs.jpg" alt="Bateau 5"  id="myCarouseli5">
      <div class="carousel-caption">
          <h3>Bateau 5</h3>
          <p>Description</p>
        </div>
    </div>
    <div class="item">
      <img src="../images/Bateaux/DnovrYvX0AENSIZ.jpg" alt="Bateau 6" id="myCarouseli6">
      <div class="carousel-caption">
          <h3>Bateau 6</h3>
          <p>Description</p>
        </div>
    </div>
    <div class="item">
      <img src="../images/Bateaux/DnZmpnsXsAE30Jf.jpg" alt="Bateau 7" id="myCarouseli7">
      <div class="carousel-caption">
          <h3>Bateau 7</h3>
          <p>Description</p>
        </div>
    </div>
    <div class="item">
      <img src="../images/Bateaux/Le_titanic.jpg" alt="Bateau 8" id="myCarouseli8">
      <div class="carousel-caption">
          <h3>Bateau 8</h3>
          <p>Description</p>
        </div>
    </div>
  </div>
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

  <div class="row" style="background-color:rgb(255, 255, 255); width: 100%; margin: 0; margin-bottom: -50px;  font-family: 'roboto'; font-size: 12px; padding-bottom: 15px; ">
  <div class="col-md-4">
  <!-- ajout de la timeline twitter de l'armada -->
    <a class="twitter-timeline" data-width="500" data-height="650" href="https://twitter.com/ArmadaRouen?lang=fr">Tweets by Armada</a> <script async src="https://platform.twitter.com/widgets.js"></script>

    </div>
    <div class="col-md-6"> 
    <p><br/></p>
    <!-- Texte descriptif -->
    <h2><b>Qu'est ce que l'Armada :</b></h2>
    <p class="text-primary">L'Armada est un large rassemblement de grands voiliers organisé à Rouen,
    dans la Seine-Maritime. Il est un des évènements importants du monde de la mer avec les Tall Ships' Races </p>
  <p class="text-primary">  Le Sail Bremerhaven (Allemagne)</p>
  <p class="text-primary">Le Sail Amsterdam (Pays-Bas)</p>
  <p class="text-primary">La Kieler Woche (Allemagne)</p>
  <p class="text-primary">La DelfSail (nl) à Delfzijl (Pays-Bas).</p>
  <p class="text-primary">Il a lieu tous les quatre à six ans sur les quais de la Seine, au sein même de la métropole normande. Cette manifestation dure en général une dizaine de jours. </p>
  <p><br/></p>
  

<h2><b>Qu'est ce qui est différent avec notre septième édition (2019) :</b></h2>
<p class="text-primary">La septième édition se déroulera du 6 au 16 juin 2019 sur les quais de Rouen, cette manifestation fêtera ses 30 ans d'existence.</p>
<p class="text-primary"> Elle verra aussi le départ de la Liberty Tall Ships Regatta 2019.</p>
<p class="text-primary">L'Armada 2019  recevra de grands voiliers et proposera diverses animations avec entre autre un invité de marque :<a href="./invites.php"> Mr. Christophe Prazuck </a>!</p>
<p><br/></p>
<a class="btn btn-outline-default waves-effect btn-block" href="./programme.php"><p>Cliquer ici pour acceder au programme complet</p></a>

</div>
</div>

<!-- FOOTER -->
<?php
include 'footer.inc.php';
?>


 