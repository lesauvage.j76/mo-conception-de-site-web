<!-- Cette page a ete générée -->
<?php
$titre_page="COM023";
include 'header.inc.php';
include 'head.inc.php';
$blaz = "./img/" . $titre_page . ".jpg";
?>
<div class="row">
<div class="col-sm-7 border border-light p-5">
<img src="<?php echo $blaz; ?>" alt="mon_bateau" style="width:100%;">
</div>
<div class="col-sm-4">
<br><br/>
<br><br/>
<p><b>Pays de construction :</b> <?php get_pay_bateaum($titre_page); ?></p>
<p><b>Nom du bateau :</b> <?php get_nom_bateaum($titre_page);?></P>
<p><b>Matricule :</b> <?php echo $titre_page;?></P>
<p><b>Date de fabrication : </b><?php get_fdate_bateaum($titre_page);?></P>
<p><b>Date d'arrivée : </b><?php get_darrivee_bateaum($titre_page);?> </P>
<p><b>Date de départ :</b><?php get_ddepart_bateaum($titre_page);?></P>
<p><b>Description du bateau:</b><?php get_description_bateaum($titre_page);?></p>
<p><b>le pdf : </b></p>
<?php
if(!isset($_COOKIE['ArmadaLogin']))
{
?>
<button type="button" class="btn btn-info btn-block" id="butt1" data-toggle="modal" data-target="#myModal1"> <p><i class="fa fa-download fa-2x mr-3" aria-hidden="true"></i>Obtenir un PDF</p></button>
<form method="post"  action="conn.php">
<div class="modal" id="myModal1">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header text-center">
<h2 class="modal-title w-100 font-weight-bold">Sign in</h2>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body mx-3">
<div class="md-form mb-5">
<i class="fa fa-envelope prefix grey-text"></i>
<input type="email" id="defaultForm-email" class="form-control validate" name="login">
<label data-error="wrong" data-success="right" for="defaultForm-email">Email</label>
</div>
<div class="md-form mb-4">
<i class="fa fa-lock prefix grey-text"></i>
<input type="password" id="defaultForm-pass" class="form-control validate" name="password">
<label data-error="wrong" data-success="right" for="defaultForm-pass">LE MOT DE PASSE</label>
</div>
</div>
<div class="modal-footer d-flex justify-content-center">
<button class="btn btn-default" type="submit">Se Connecter</button>
<a href="./inscription.php" class="btn btn-info" role="button">S'inscrire</a>
</div>
</div>
</div>
</div>
</form>
<?php
}
else
{
?>
<a href="./create_pdfm.php?mat=<?php echo $titre_page;?>" class="btn btn-info btn-block" role="button"><p>OBTENIR UN PDF</p></a>
<?php
}?>
</div>
</div>
<div class="col-sm-1"> </div>
<?php
include 'footer.inc.php';
?>
