<!-- INCLUDE -->
<?php 
include_once 'fonction.php';

if(isset($_COOKIE['ArmadaLogin']))//si il n'est pas connecté l'utilisateur a la navbar de base
{
    header('Location: index.php');
}
$titre_page="Connexion";?>
<?php include 'header.inc.php';?>
<?php include 'head.inc.php';?>
     <div class="container">
            <div class="col-md-1">
            </div>
            <div class="col-md-10">
            <p> <br /> </p>
               <h2 style="text-align:center;" >Connexion</h2> 
                <form class="form-horizontal" method="post"  action="conn.php">
                    <div class="form-group">
                      <label class="control-label " for="email">Email:</label>
                      
                        <input type="email" class="form-control" id="email" placeholder="Email"  name="login" maxlength="40" required>
                      </div>
                    
                    <div class="form-group">
                      <label class="control-label " for="pwd">Mot de passe:</label>
                     
                        <input type="password" class="form-control" id="pwd" placeholder="Password" name="password" maxlength="255" required>
                      </div>
                   
                    <div class="form-group">
                     
                       <button type="submit" class="btn btn-primary btn-block ">Connexion</button>
      
                    </div>
                    <div class="form-group">
                   
                      <a href="./Inscription.php" class="btn btn-info btn-block btn-lg" role="button">S'inscrire</a>
                
                    </div>
                  </form> 
            </div>
        </div>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/mdb.min.js"></script>
    <?php include 'footer.inc.php'; ?>