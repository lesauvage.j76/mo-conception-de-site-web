<nav class="navbar navbar-expand-lg navbar-dark primary-color sticky-top nav-collapse " id="navbar">
  <a class="navbar-brand" href="./index.php">ARMADA 2019</a>
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="./index.php"><p>Accueil</p>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="./programme.php"><p>Programme</p></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="./info_pratique.php"><p>Info pratique</p></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="./voir_tous_les_bateaux.php"><p>Les Bateaux</p></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="./sponsors.php"><p>Sponsors</p></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="./invites.php"><p>Invites</p></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="./admin.php"><p>Page admin</p></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="./disconnect.php"><p>Deconnexion</p></a>
      </li>
    </ul>
</nav>
<div style="height:25px;display:block;"> </div>
