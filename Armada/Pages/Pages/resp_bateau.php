<?php
include_once 'fonction.php';

    if(!isset($_COOKIE['ArmadaLogin']))
    {
        include 'invalid_rights.php';
    }
    else
    {
        $batal = GetUtilisaateur($_COOKIE['ArmadaLogin']);
        if ($batal == 0) {//0 : pas de bateau, redirection vers la page ajouter un bateau
            //include 'ajout_bateau.php';
            header('Location: ajout_bateau.php');

        }
        if ($batal == 1) {//1 : il posséde un bateau, redirection vers la page ou les information de son bateau sont affiché, et permet de les modifier si nécessaire
           // include 'mon_bateau.php';
           header('Location: mon_bateau.php');
        }
    }
?>