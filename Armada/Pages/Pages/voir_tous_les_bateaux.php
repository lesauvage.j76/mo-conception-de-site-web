<!-- INCLUDE -->
<?php
$titre_page="Voir tous les bateaux";
include 'header.inc.php';
include 'head.inc.php';
include_once 'fonction.php';

$directory = "./img";
$images = glob($directory . "/*.jpg");
?>

<div class="gallery" id="crow">
  <?php
       foreach($images as $image) {
          $pinfo = pathinfo($image, PATHINFO_BASENAME);
          $rest = substr($image, 0, -4);
          $rest = substr($rest, 6);
          $fil = "./" . $rest . ".php";
  ?>
  <div class="view overlay">
    <a href="<?php echo $fil; ?>">
        <img src="<?php echo $image; ?>" alt="<?php echo $image; ?>" style="width:auto; height:300px;" class="img-fluid hoverable">
        <div class="mask flex-center waves-effect waves-light rgba-blue-strong">
          
          <p class="white-text"><?php echo get_nom_bateaum($rest); ?></p>
        </div>
    </a>
  </div>
  <?php
       }
  ?>
</div>

<!-- FOOTER -->
<?php
include 'footer.inc.php';
?>


 