<?php
$titre_page="Create PDF";
include_once 'fonction.php';

define('FPDF_FONTPATH','./font');
require('fpdf.php');
 
$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('times','B',38);
$pdf->text(10,20, get_nom_bateau_mb($_COOKIE['ArmadaLogin']));// x, y

$pdf->SetFont('times','B',20);
$pdf->text(70,35,'Information : ');

$pdf->SetFont('times','B',12);

$pdf->text(10,45,'Pays de construction : '.get_pays_bateau_mb($_COOKIE['ArmadaLogin']));
$pdf->text(10,55,'Matricule : '.get_matricule_bateau_mb($_COOKIE['ArmadaLogin']));
$pdf->text(10,65,'Date de fabrication : '.get_fdate_bateau_mb($_COOKIE['ArmadaLogin']));
$pdf->text(10,85,'Date d\'arrivee : '.get_darrivee_bateau_mb($_COOKIE['ArmadaLogin']));
$pdf->text(10,95,'Date de depart : '.get_ddepart_bateau_mb($_COOKIE['ArmadaLogin']));
$pdf->text(10,105,'Description : '.get_description_bateau_mb($_COOKIE['ArmadaLogin']));

$pdf->SetFont('times','B',20);
$pdf->text(60,115,'Information detaille : ');

$pdf->SetFont('times','B',12);
$pdf->text(10,125,'Nombre d\'equipage : '.get_nbequipage_bateau_mb($_COOKIE['ArmadaLogin']));
$pdf->text(10,135,'Nombre de cabine : '.get_nbcabine_bateau_mb($_COOKIE['ArmadaLogin']));
$pdf->text(10,145,'Nombre de passager: '.get_nbpassager_bateau_mb($_COOKIE['ArmadaLogin']));
$pdf->text(10,155,'Port d\'attache : '.get_port_bateau_mb($_COOKIE['ArmadaLogin']));
$pdf->text(10,165,'Vitesse max : '.get_vitesse_bateau_mb($_COOKIE['ArmadaLogin']));
$pdf->text(10,175,'Chantier de construction : '.get_chantier_bateau_mb($_COOKIE['ArmadaLogin']));
$pdf->text(10,185,'Longueur : '.get_longueur_bateau_mb($_COOKIE['ArmadaLogin']).' m');
$pdf->text(10,195,'Poids : '.get_poids_bateau_mb($_COOKIE['ArmadaLogin']).' t');

$pdf->Output("D", get_nom_bateau_mb($_COOKIE['ArmadaLogin']).".pdf");
?>