<?php
include_once 'fonction.php';

echo $_POST['user'];
if ($_POST['user'] != "default")
{
    $rol = 0;
    switch ($_POST['role']) {
        case "zero":
            $rol = 0;
            break;
        case "un":
            $rol = 1;
            break;
        case "deux":
            $rol = 2;
            break;
        case "trois":
            delete_user($_POST['user']);
            header('Location: admin.php');
            break;
    }
    echo $rol;
    update_user($_POST['user'], $rol);
}

header('Location: admin.php');
?>  