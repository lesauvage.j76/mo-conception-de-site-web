<?php
include_once 'fonction.php';

//Utilisation de la fonction pour ajouter le bateau a la BDD
$info = $_FILES['imae'];
$src = $info['tmp_name'];
$dest = "./img/" . $_POST['matricule'] . ".jpg";
echo $dest;
echo $src;
copy($src, $dest);
post_boat($_POST['nom'], $_POST['pays'], $_POST['matricule'], $_POST['fdate'], $_POST['longueur'], $_POST['poids'], $_POST['desc'], $_POST['cabine'], $_POST['passager'], $_POST['port'], $_POST['bspeed'], $_POST['chantier'], $_POST['date_arrivee'], $_POST['date_fin']);

// Boat Pages Fuzzer
$file_to_generate = "./". $_POST['matricule'] . ".php";
$generate_file = fopen($file_to_generate, "w");
$txt = "<!-- Cette page a ete générée -->\n";
fwrite($generate_file, $txt);
$txt = "<?php\n";
fwrite($generate_file, $txt);
$txt = "\$titre_page=\"" . $_POST['matricule'] . "\";\n";
fwrite($generate_file, $txt);
$txt = "include 'header.inc.php';\n";
fwrite($generate_file, $txt);
$txt = "include 'head.inc.php';\n";
fwrite($generate_file, $txt);
$txt = "\$blaz = \"./img/\" . \$titre_page . \".jpg\";\n";
fwrite($generate_file, $txt);
$txt = "?>\n";
fwrite($generate_file, $txt);
$txt = "<div class=\"row\">\n";
fwrite($generate_file, $txt);
$txt = "<div class=\"col-sm-7 border border-light p-5\">\n";
fwrite($generate_file, $txt);
$txt = "<img src=\"<?php echo \$blaz; ?>\" alt=\"mon_bateau\" style=\"width:100%;\">\n";
fwrite($generate_file, $txt);
$txt = "</div>\n";
fwrite($generate_file, $txt);
$txt = "<div class=\"col-sm-4\">\n";
fwrite($generate_file, $txt);
$txt = "<br><br/>\n";
fwrite($generate_file, $txt);
$txt = "<br><br/>\n";
fwrite($generate_file, $txt);
$txt = "<p><b>Pays de construction :</b> <?php get_pay_bateaum(\$titre_page); ?></p>\n";
fwrite($generate_file, $txt);
$txt = "<p><b>Nom du bateau :</b> <?php get_nom_bateaum(\$titre_page);?></P>\n";
fwrite($generate_file, $txt);
$txt = "<p><b>Matricule :</b> <?php echo \$titre_page;?></P>\n";
fwrite($generate_file, $txt);
$txt = "<p><b>Date de fabrication : </b><?php get_fdate_bateaum(\$titre_page);?></P>\n";
fwrite($generate_file, $txt);
$txt = "<p><b>Date d'arrivée : </b><?php get_darrivee_bateaum(\$titre_page);?> </P>\n";
fwrite($generate_file, $txt);
$txt = "<p><b>Date de départ :</b><?php get_ddepart_bateaum(\$titre_page);?></P>\n";
fwrite($generate_file, $txt);
$txt = "<p><b>Description du bateau:</b><?php get_description_bateaum(\$titre_page);?></p>\n";
fwrite($generate_file, $txt);
$txt = "<p><b>le pdf : </b></p>\n";
fwrite($generate_file, $txt);
$txt = "<?php\nif(!isset(\$_COOKIE['ArmadaLogin']))\n{\n?>\n<button type=\"button\" class=\"btn btn-info btn-block\" id=\"butt1\" data-toggle=\"modal\" data-target=\"#myModal1\"> <p><i class=\"fa fa-download fa-2x mr-3\" aria-hidden=\"true\"></i>Obtenir un PDF</p></button>\n<form method=\"post\"  action=\"conn.php\">\n<div class=\"modal\" id=\"myModal1\">\n<div class=\"modal-dialog\" role=\"document\">\n<div class=\"modal-content\">\n<div class=\"modal-header text-center\">\n<h2 class=\"modal-title w-100 font-weight-bold\">Sign in</h2>\n<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n<span aria-hidden=\"true\">&times;</span>\n</button>\n</div>\n<div class=\"modal-body mx-3\">\n<div class=\"md-form mb-5\">\n<i class=\"fa fa-envelope prefix grey-text\"></i>\n<input type=\"email\" id=\"defaultForm-email\" class=\"form-control validate\" name=\"login\">\n<label data-error=\"wrong\" data-success=\"right\" for=\"defaultForm-email\">Email</label>\n</div>\n<div class=\"md-form mb-4\">\n<i class=\"fa fa-lock prefix grey-text\"></i>\n<input type=\"password\" id=\"defaultForm-pass\" class=\"form-control validate\" name=\"password\">\n<label data-error=\"wrong\" data-success=\"right\" for=\"defaultForm-pass\">LE MOT DE PASSE</label>\n</div>\n</div>\n<div class=\"modal-footer d-flex justify-content-center\">\n<button class=\"btn btn-default\" type=\"submit\">Se Connecter</button>\n<a href=\"./Inscription.php\" class=\"btn btn-info\" role=\"button\">S'inscrire</a>\n</div>\n</div>\n</div>\n</div>\n</form>\n<?php\n}\nelse\n{\n?>\n<a href=\"./create_pdfm.php?mat=<?php echo \$titre_page;?>\" class=\"btn btn-info btn-block\" role=\"button\"><p>OBTENIR UN PDF</p></a>\n<?php\n}?>\n";
fwrite($generate_file, $txt);
$txt = "</div>\n";
fwrite($generate_file, $txt);
$txt = "</div>\n";
fwrite($generate_file, $txt);
$txt = "<div class=\"col-sm-1\"> </div>\n";
fwrite($generate_file, $txt);
$txt = "<?php\n";
fwrite($generate_file, $txt);
$txt = "include 'footer.inc.php';\n";
fwrite($generate_file, $txt);
$txt = "?>\n";
fwrite($generate_file, $txt);
fclose($file_to_generate);

header('Location: index.php'); //retour a l'accueil 
?>