<!-- INCLUDE -->
<?php
include_once 'fonction.php';

if(isset($_COOKIE['ArmadaLogin']))//si il n'est pas connecté l'utilisateur a la navbar de base
{
    header('Location: index.php');
}
$titre_page="Inscription";
include 'header.inc.php';
include 'head.inc.php';

?>
<form class="text-center border border-light p-5" method="post"  action="register.php"><!--Le formulaire serat traité dans la page register.php-->
<p class="h4 mb-4">Sign up</p>
<div class="form-row mb-4">
    <div class="col">
        <input type="text" id="defaultRegisterFormFirstName" class="form-control" placeholder="First name" name="prenom" pattern="^[A-Za-z -]*$" maxlength="20" required>
    </div>
    <div class="col">
        <input type="text" id="defaultRegisterFormLastName" class="form-control" placeholder="Last name" name="nom" pattern="^[A-Za-z -]*$" maxlength="20" required>
    </div>
</div>
Birthday: <input type="date" name="bdate">
<input type="email" id="defaultRegisterFormEmail" class="form-control mb-4" placeholder="E-mail" name="mail" maxlength="40" required>
<input type="password" id="defaultRegisterFormPassword" class="form-control" placeholder="Password" aria-describedby="defaultRegisterFormPasswordHelpBlock" name="mdp" maxlength="255" required>
<small id="defaultRegisterFormPasswordHelpBlock" class="form-text text-muted mb-4">
  
</small>
<button class="btn btn-info my-4 btn-block" type="submit">Sign in</button>
<hr>
</form>

<!-- FOOTER -->
<?php
include 'footer.inc.php';
?>


 