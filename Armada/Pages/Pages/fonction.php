<?php
// CONNECTION 
function Armada_Connection()
{
    $con = mysqli_connect('127.0.0.1', 'root', 'root', 'armada');
    //$con = mysqli_connect('localhost', 'grp_7_3', 'foowel2Quo', 'bdd_7_3');
    if (!$con) {
        echo "Connect Error: " . mysqli_connect_error();
        exit();
    }
    return $con;
}

//Fonction de la page admin
function GetUtilisateurF($login)
{
  $con = Armada_Connection();
  $sql = 'SELECT FONCTION FROM personne where MAIL = \''.$login.'\''; 
  $query  = mysqli_query($con, $sql); 
  $rowCount = mysqli_num_rows($query);
  mysqli_close($con);
  if ($rowCount == 0) {
    return null;
  }
  $fetch = mysqli_fetch_assoc($query);
  return $fetch['FONCTION'];
}

function afficher_personne(){
    $con = Armada_Connection(); 
    $sql = 'SELECT * FROM personne  GROUP BY NOM'; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    if ($rowCount == 0) {
        echo "Pas d'utilisateur";
    }
    $arr = [];
  while($row = $query->fetch_array())
  {
    array_push($arr, $row);
  }
  return $arr;
}

function afficher_bateau(){
    $con = Armada_Connection(); 
    $sql = 'SELECT * FROM bateau  GROUP BY NOM_BATEAU'; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    if ($rowCount == 0) {
        echo "Pas de bateau";
    }
    $arr = [];
  while($row = $query->fetch_array())
  {
  array_push($arr, $row);
  }
  return $arr;
  }

  function row_countp(){
    $con = Armada_Connection(); 
    $sql = 'SELECT * FROM personne  GROUP BY NOM'; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    return $rowCount;
  }
  
  function row_count(){
    $con = Armada_Connection(); 
    $sql = 'SELECT * FROM bateau  GROUP BY NOM_BATEAU'; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    return $rowCount;
  }

//Fonction de la page boat_register_inc 
  function get_matricule_b($login){
    $con = Armada_Connection(); 
    $sql = 'SELECT MATRICULE FROM bateau  WHERE MAIL_CAPITAINE = \''.$login.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    $fetch = mysqli_fetch_assoc($query);
    return $fetch['MATRICULE'];
  }

  //Fonction pour la page boat_register
  function post_boat($nom, $pays, $matricule, $fdate, $longueur, $poids, $desc, $cabine, $passager, $port, $speed, $chantier, $date_arrivee, $date_fin) // ajouter une bateau a la BDD
  {
      $con = Armada_Connection();
      $cook = $_COOKIE['ArmadaLogin']; // on recupere l'addresse mail de l'utilisateur que l'on stock dans $cook
      $sql = "INSERT INTO bateau(NOM_BATEAU, MAIL_CAPITAINE, PAY_CONSTRUCTION, MATRICULE, DATE_FABRICATION, LONGUEUR, POIDS, ARMADA_PRECEDENT, 
                                  DESCRIPTION_BATEAU, NB_CABINE, NB_PASSAGER, PORT_ATTACHE, VITESSE_MAX, NB_EQUIPAGE,
                                  CHANTIER, DATE_ARRIVEE, DATE_DEPART) 
      VALUES ('$nom', '$cook', '$pays', '$matricule', '$fdate', '$longueur', '$poids', 1997, '$desc', '$cabine', '$passager', '$port', '$speed', 5, '$chantier', '$date_arrivee', '$date_fin')";
      $query = mysqli_query($con, $sql);
      echo $sql;
      mysqli_close($con);
      $query;
  }

//Fonction pour la page conn
function GetMdp($login)
{
    $con = Armada_Connection();
    $sql = 'SELECT MDP FROM personne where MAIL = \''.$login.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    if ($rowCount == 0) {
        return null;
    }
    $fetch = mysqli_fetch_assoc($query);
    return $fetch['MDP'];
}

//Fonction pour la page fonction_get_bateau.inc.php
function get_pays_bateau_mb($login){
    $con = Armada_Connection(); 
    $sql = 'SELECT PAY_CONSTRUCTION FROM bateau  WHERE MAIL_CAPITAINE = \''.$login.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    $fetch = mysqli_fetch_assoc($query);
      return $fetch['PAY_CONSTRUCTION'];
  }
  
  function get_matricule_bateau_mb($login){
    $con = Armada_Connection(); 
    $sql = 'SELECT MATRICULE FROM bateau  WHERE MAIL_CAPITAINE = \''.$login.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    $fetch = mysqli_fetch_assoc($query);
      return $fetch['MATRICULE'];
  }
  
  function get_fdate_bateau_mb($login){
    $con = Armada_Connection(); 
    $sql = 'SELECT DATE_FABRICATION FROM bateau  WHERE MAIL_CAPITAINE = \''.$login.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    $fetch = mysqli_fetch_assoc($query);
      return $fetch['DATE_FABRICATION'];
  }
  
  function get_armada_bateau_mb($login){
    $con = Armada_Connection(); 
    $sql = 'SELECT ARMADA_PRECEDENT FROM bateau  WHERE MAIL_CAPITAINE = \''.$login.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    $fetch = mysqli_fetch_assoc($query);
      return $fetch['ARMADA_PRECEDENT'];
  }
  
  function get_darrivee_bateau_mb($login){
    $con = Armada_Connection(); 
    $sql = 'SELECT DATE_ARRIVEE FROM bateau  WHERE MAIL_CAPITAINE = \''.$login.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    $fetch = mysqli_fetch_assoc($query);
      return $fetch['DATE_ARRIVEE'];
  }
  
  function get_ddepart_bateau_mb($login){
    $con = Armada_Connection(); 
    $sql = 'SELECT DATE_DEPART FROM bateau  WHERE MAIL_CAPITAINE = \''.$login.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    $fetch = mysqli_fetch_assoc($query);
      return $fetch['DATE_DEPART'];
  }
  function get_nom_bateau_mb($login){
    $con = Armada_Connection(); 
    $sql = 'SELECT NOM_BATEAU FROM bateau  WHERE MAIL_CAPITAINE = \''.$login.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    $fetch = mysqli_fetch_assoc($query);
      return $fetch['NOM_BATEAU'];
  }

  function get_login_mb($mat){
    $con = Armada_Connection(); 
    $sql = 'SELECT MAIL_CAPITAINE FROM bateau  WHERE MATRICULE = \''.$mat.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    $fetch = mysqli_fetch_assoc($query);
      return $fetch['MAIL_CAPITAINE'];
  }
  
  function get_description_bateau_mb($login){
    $con = Armada_Connection(); 
    $sql = 'SELECT DESCRIPTION_BATEAU FROM bateau  WHERE MAIL_CAPITAINE = \''.$login.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    $fetch = mysqli_fetch_assoc($query);
      return $fetch['DESCRIPTION_BATEAU'];
  }

  function get_longueur_bateau_mb($login){
    $con = Armada_Connection(); 
    $sql = 'SELECT LONGUEUR FROM bateau  WHERE MAIL_CAPITAINE = \''.$login.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    $fetch = mysqli_fetch_assoc($query);
      return $fetch['LONGUEUR'];
  }
  
  function get_poids_bateau_mb($login){
    $con = Armada_Connection(); 
    $sql = 'SELECT POIDS FROM bateau  WHERE MAIL_CAPITAINE = \''.$login.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    $fetch = mysqli_fetch_assoc($query);
      return $fetch['POIDS'];
  }
  
  function get_nbcabine_bateau_mb($login){
    $con = Armada_Connection(); 
    $sql = 'SELECT NB_CABINE FROM bateau  WHERE MAIL_CAPITAINE = \''.$login.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    $fetch = mysqli_fetch_assoc($query);
      return $fetch['NB_CABINE'];
  }
  
  function get_nbpassager_bateau_mb($login){
    $con = Armada_Connection(); 
    $sql = 'SELECT NB_PASSAGER FROM bateau  WHERE MAIL_CAPITAINE = \''.$login.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    $fetch = mysqli_fetch_assoc($query);
      return $fetch['NB_PASSAGER'];
  }
  
  function get_port_bateau_mb($login){
    $con = Armada_Connection(); 
    $sql = 'SELECT PORT_ATTACHE FROM bateau  WHERE MAIL_CAPITAINE = \''.$login.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    $fetch = mysqli_fetch_assoc($query);
      return $fetch['PORT_ATTACHE'];
  }
  
  function get_vitesse_bateau_mb($login){
    $con = Armada_Connection(); 
    $sql = 'SELECT VITESSE_MAX FROM bateau  WHERE MAIL_CAPITAINE = \''.$login.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    $fetch = mysqli_fetch_assoc($query);
      return $fetch['VITESSE_MAX'];
  }
  
  function get_nbequipage_bateau_mb($login){
    $con = Armada_Connection(); 
    $sql = 'SELECT NB_EQUIPAGE FROM bateau  WHERE MAIL_CAPITAINE = \''.$login.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    $fetch = mysqli_fetch_assoc($query);
      return $fetch['NB_EQUIPAGE'];
  }
  
  function get_chantier_bateau_mb($login){
    $con = Armada_Connection(); 
    $sql = 'SELECT CHANTIER FROM bateau  WHERE MAIL_CAPITAINE = \''.$login.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    $fetch = mysqli_fetch_assoc($query);
      return $fetch['CHANTIER'];
  }
  
//Fonction de la page head.inc.php
function GetUtilisateur($login)
{
    $con = Armada_Connection();
    $sql = 'SELECT FONCTION FROM personne where MAIL = \''.$login.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    if ($rowCount == 0) {
        return null;
    }
    $fetch = mysqli_fetch_assoc($query);
    return $fetch['FONCTION'];
}

//Fonction de la page mon_bateau.php
function get_nom_bateau($login){
    $con = Armada_Connection(); 
    $sql = 'SELECT NOM_BATEAU FROM bateau  WHERE MAIL_CAPITAINE = \''.$login.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
  while($row = $query->fetch_array())
  {
  echo $row['NOM_BATEAU'];
  }
  }

  function get_nom_bateaum($matricule){
    $con = Armada_Connection(); 
    $sql = 'SELECT NOM_BATEAU FROM bateau  WHERE MATRICULE = \''.$matricule.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
  while($row = $query->fetch_array())
  {
  echo $row['NOM_BATEAU'];
  }
  }

  function get_pay_bateau($login){
    $con = Armada_Connection(); 
    $sql = 'SELECT PAY_CONSTRUCTION FROM bateau  WHERE MAIL_CAPITAINE = \''.$login.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
  while($row = $query->fetch_array())
  {
  echo $row['PAY_CONSTRUCTION'];
  }
  }

  function get_pay_bateaum($matricule){
    $con = Armada_Connection(); 
    $sql = 'SELECT PAY_CONSTRUCTION FROM bateau  WHERE MATRICULE = \''.$matricule.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
  while($row = $query->fetch_array())
  {
  echo $row['PAY_CONSTRUCTION'];
  }
  }

  
  function get_matricule_bateau($login){
    $con = Armada_Connection(); 
    $sql = 'SELECT MATRICULE FROM bateau  WHERE MAIL_CAPITAINE = \''.$login.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
  while($row = $query->fetch_array())
  {
  echo $row['MATRICULE'];
  }
  }
  
  function get_fdate_bateau($login){
    $con = Armada_Connection(); 
    $sql = 'SELECT DATE_FABRICATION FROM bateau  WHERE MAIL_CAPITAINE = \''.$login.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
  while($row = $query->fetch_array())
  {
  echo $row['DATE_FABRICATION'];
  }
  }

  function get_fdate_bateaum($matricule){
    $con = Armada_Connection(); 
    $sql = 'SELECT DATE_FABRICATION FROM bateau  WHERE MATRICULE = \''.$matricule.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
  while($row = $query->fetch_array())
  {
  echo $row['DATE_FABRICATION'];
  }
  }
  
  function get_armada_bateau($login){
    $con = Armada_Connection(); 
    $sql = 'SELECT ARMADA_PRECEDENT FROM bateau  WHERE MAIL_CAPITAINE = \''.$login.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
  while($row = $query->fetch_array())
  {
  echo $row['ARMADA_PRECEDENT'];
  }
  }
  
  function get_darrivee_bateau($login){
    $con = Armada_Connection(); 
    $sql = 'SELECT DATE_ARRIVEE FROM bateau  WHERE MAIL_CAPITAINE = \''.$login.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
  while($row = $query->fetch_array())
  {
  echo $row['DATE_ARRIVEE'];
  }
  }

  function get_darrivee_bateaum($matricule){
    $con = Armada_Connection(); 
    $sql = 'SELECT DATE_ARRIVEE FROM bateau  WHERE MATRICULE = \''.$matricule.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
  while($row = $query->fetch_array())
  {
  echo $row['DATE_ARRIVEE'];
  }
  }
  
  function get_ddepart_bateau($login){
    $con = Armada_Connection(); 
    $sql = 'SELECT DATE_DEPART FROM bateau  WHERE MAIL_CAPITAINE = \''.$login.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
  while($row = $query->fetch_array())
  {
  echo $row['DATE_DEPART'];
  }
  }

  function get_ddepart_bateaum($matricule){
    $con = Armada_Connection(); 
    $sql = 'SELECT DATE_DEPART FROM bateau  WHERE MATRICULE = \''.$matricule.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
  while($row = $query->fetch_array())
  {
  echo $row['DATE_DEPART'];
  }
  }
  
  function get_description_bateau($login){
    $con = Armada_Connection(); 
    $sql = 'SELECT DESCRIPTION_BATEAU FROM bateau  WHERE MAIL_CAPITAINE = \''.$login.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
  while($row = $query->fetch_array())
  {
  echo $row['DESCRIPTION_BATEAU'];
  }
  }
  
  function get_description_bateaum($matricule){
    $con = Armada_Connection(); 
    $sql = 'SELECT DESCRIPTION_BATEAU FROM bateau  WHERE MATRICULE = \''.$matricule.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
  while($row = $query->fetch_array())
  {
  echo $row['DESCRIPTION_BATEAU'];
  }
  }
  // Fonction  de la page register.php
  function post_user($nom, $prenom, $bdate, $mail, $mdp, $fonction)
{
    $con = Armada_Connection();
    $sql = "INSERT INTO personne(NOM, PRENOM, DATE_NAISSANCE, MAIL, MDP, FONCTION) 
        VALUES ('$nom', '$prenom', '$bdate', '$mail', '$mdp', '$fonction')";
    $query = mysqli_query($con, $sql);
    mysqli_close($con);
    $query;
}

function update_user($mail, $func)
{
  $con = Armada_Connection();
  $sql = "UPDATE personne SET FONCTION = '$func' WHERE personne.MAIL = '$mail'";
  $query = mysqli_query($con, $sql);
  mysqli_close($con);
  $query;
}

function delete_user($mail)
{
  $con = Armada_Connection();
  $matri = get_matricule_bateau_mb($mail);
  delete_boat($matri);
  $sql = "DELETE FROM personne WHERE personne.MAIL = '$mail'";
  $query = mysqli_query($con, $sql);
  mysqli_close($con);
  $query;
}

function delete_boat($matri)
{
  $con = Armada_Connection();
  unlink("./img/" . $matri . ".jpg");
  unlink("./" . $matri . ".php");
  $sql = "DELETE FROM bateau WHERE bateau.MATRICULE = '$matri'";
  $query = mysqli_query($con, $sql);
  mysqli_close($con);
  $query;
}

//Fonction de la page resp_bateau
function GetUtilisaateur($login)
{
    $con = Armada_Connection();
    $sql = 'SELECT * FROM bateau where MAIL_CAPITAINE = \''.$login.'\''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    return $rowCount;
}
?>