<!-- INCLUDE -->
<?php
include_once 'fonction.php';

$func = GetUtilisateurF($_COOKIE['ArmadaLogin']);
if(!isset($_COOKIE['ArmadaLogin']) || $func == 0)//si il n'est pas connecté l'utilisateur a la navbar de base
{
    header('Location: index.php');
}
$titre_page="Mon bateau";
include 'header.inc.php';
include 'head.inc.php';


$blaz = "./img/" . get_matricule_b($_COOKIE['ArmadaLogin']) . ".jpg";
?>

<!-- AFFICHAGE PAGE -->
 <div class="container-fluid">
  <div class="col-sm-7 border border-light p-5">
  <h1><b>Mon bateau : </b></h1>
  <img src="<?php echo $blaz; ?>" alt="mon_bateau" style="width:100%;">
  </div>
  <div class="col-sm-4">
  <br><br/>
  <br><br/>   
  <p><b>Pays de construction : </b><?php get_pay_bateau($_COOKIE['ArmadaLogin']);?> <a href="#" role="button" id="b1" ><i class="fas fa-edit bbtn"></i></a></p>
  <p><b>Nom du bateau :</b> <?php get_nom_bateau($_COOKIE['ArmadaLogin']);?><a href="#" role="button" id="b1"><i class="fas fa-edit bbtn"></i></a></p>
  <p><b>Matricule :</b> <?php get_matricule_bateau($_COOKIE['ArmadaLogin']);?></p>
  <p><b>Date de fabrication : </b><?php get_fdate_bateau($_COOKIE['ArmadaLogin']);?><a href="#" role="button" id="b1"><i class="fas fa-edit bbtn"></i></a></p>
  <p><b>Date d'arrivée : </b><?php get_darrivee_bateau($_COOKIE['ArmadaLogin']);?><a href="#" role="button" id="b1"><i class="fas fa-edit bbtn"></i></a></p>
  <p><b>Date de départ :</b><?php get_ddepart_bateau($_COOKIE['ArmadaLogin']);?><a href="#" role="button" id="b1"><i class="fas fa-edit bbtn"></i></a></p>
  <p><b>Description du bateau : </b></p><p><?php get_description_bateau($_COOKIE['ArmadaLogin']);?><a href="#" role="button" id="b1"><i class="fas fa-edit bbtn"></i></a></p>
  <p><b>le pdf : </b></p>
  <a href="./create_pdf.php" class="btn btn-info btn-block" role="button"><p><i class="fa fa-download fa-2x mr-3" aria-hidden="true"></i>Obtenir un PDF</p></a>
  <br>
</div>
</div>
<div class="col-sm-1"> </div>



<!-- FOOTER -->
<?php
include 'footer.inc.php';
?>
