<!-- INCLUDE -->

<?php 
include_once 'fonction.php';
 if(!isset($_COOKIE['ArmadaLogin']))
 {
     header('Location: index.php');
 }
 else
 {
     $func = GetUtilisateurF($_COOKIE['ArmadaLogin']);
     if ($func == 0 || $func == 1) { //0 correspond au niveau de membre le bouton connexion devient un deconnexion
         header('Location: index.php');
     }
}
$titre_page="Admin";
include 'header.inc.php';
include 'head.inc.php';
$people = afficher_personne(); 
$batal = afficher_bateau();

?>

<div class="container table-responsive">
 <!-- TABLE PERSONNE -->
 <br/>
 <br/>
 <h2><b>TABLE PERSONNE :</b></h2>
 <table id="tablePreview" class="table table-striped table-hover table-borderless">
  <thead>
    <tr>
      <th>#</th>
      <th>Nom</th>
      <th>Prenom</th>
      <th>Date de Naissance</th>
      <th>Mail</th>
      <th>Password</th>
      <th>Fonction</th>
    </tr>
  </thead>
  <tbody>
  <?php 
    for ($i = 0; $i < row_countp() ; $i++) {
  ?>
    <tr>
      <th scope="row"><?php echo $i + 1; ?></th>
      <?php 
        for ($j = 0 ; $j < 6 ; $j++){
      ?>
      <td><?php echo $people[$i][$j + 1];?></td>
      <?php 
        }
      ?>
    </tr>
  <?php 
      }
  ?>
  </tbody>
</table>
<br/>
<br/>

<form class="text-center border border-light p-5" method="post"  action="change.php">
<p class="h4 mb-4">Modifier utilisateur</p>
<div class="form-row mb-4">
    <div class="col">
    <br/>
      <select class="browser-default custom-select mb-4" id="select" name="user">
        <option value="default" selected="selected">- - - -</option>
        <?php 
          for ($i = 0; $i < row_countp() ; $i++) {
        ?>
        <option value="<?php echo $people[$i][4];?>"><?php echo $people[$i][4];?> </option>
    <?php 
      }
    ?>
      </select>
    </div>
    <div class="col">
    <br/>
    <select class="browser-default custom-select mb-4" id="select" name="role">
        <option value="zero" selected="selected">Visiteur</option>
        <option value="un">Responsable Bateau</option>
        <option value="deux">Admin</option>
        <option value="trois">Supprimer</option>
      </select>
    </div>
    <div class="col"> 
      <button class="btn btn-info my-4 btn-block btn-md" type="submit">Modifier</button>
    </div> 
</div>
</form>

<!-- TABLE BATEAU -->
<h2><b>TABLE BATEAU :</b></h2>
<table id="tablePreview" class="table table-striped table-hover table-borderless">
  <thead>
    <tr>
      <th>#</th>
      <th>Nom</th>
      <th>Capitaine</th>
      <th>Pays Construction</th>
      <th>Matricule</th>
      <th>Date Fabrication</th>
      <th>Longueur</th>
      <th>Poids</th>
      <th>Armada Precedent</th>
      <th>Description</th>
      <th>Nb. Cabine</th>
      <th>Nb. Passagers</th>
      <th>Port d'attache</th>
      <th>Vitesse max.</th>
      <th>Nom d'equipage</th>
      <th>Chantier</th>
      <th>Date arrivee</th>
      <th>Date depart</th>
    </tr>
  </thead>
  <tbody>
  <?php 
    for ($i = 0; $i < row_count() ; $i++) {
  ?>
    <tr>
      <th scope="row"><?php echo $i + 1; ?></th>
      <?php 
        for ($j = 0 ; $j < 17 ; $j++){
      ?>
      <td><?php echo $batal[$i][$j];?></td>
      <?php 
        }
      ?>
    </tr>
  <?php 
      }
  ?>
  </tbody>
</table>



<form class="text-center border border-light p-5" method="post"  action="delete_batal.php">
<p class="h4 mb-4">Supprimer bateau</p>
<div class="form-row mb-4">
    <div class="col">
    <br/>
      <select class="browser-default custom-select mb-4" id="select" name="boat">
        <option value="default" selected="selected">- - - -</option>
        <?php 
          for ($i = 0; $i < row_count() ; $i++) {
        ?>
        <option value="<?php echo $batal[$i][3];?>"><?php echo $batal[$i][3];?> </option>
    <?php 
      }
    ?>
      </select>
    </div>
    <div class="col"> 
      <button class="btn btn-danger my-4 btn-block btn-md" type="submit">Supprimer</button>
    </div> 
</div>
</form>


</div>


<!-- FOOTER -->
<?php
include 'footer.inc.php';
?>


 