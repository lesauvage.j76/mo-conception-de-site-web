<body>
<img class="topimg" src="https://www.rouentourisme.com/wp-content/uploads/2015/07/DSC_0364v2.jpg"  alt="header armada rouen">

<?php
include_once 'fonction.php';
    if(!isset($_COOKIE['ArmadaLogin']))//si il n'est pas connecté l'utilisateur a la navbar de base
    {
        include 'nav.inc.php';
    }
    else
    {
        $func = GetUtilisateur($_COOKIE['ArmadaLogin']);
        if ($func == 0) { //0 correspond au niveau de membre le bouton connexion devient un deconnexion
            include 'nav_member.inc.php';
        }
        if ($func == 1) {//1 correspond au responsable de bateau, il a un bouton espace responsable bateau en plus
            include 'nav_responsable.inc.php';
        }
        if ($func == 2) {//2 correspond a l'admin, il a la navbar la plus compléte, permettant d'accéder a la page admin
            include 'nav_admin.inc.php';
        }
    }
?>
