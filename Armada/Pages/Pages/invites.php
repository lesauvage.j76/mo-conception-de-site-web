<!-- INCLUDE -->
<?php
$titre_page="Invites";
include 'header.inc.php';
include 'head.inc.php';
?>

<div class="container">
  <!-- 1ere personne -->
  <div class="column" id="idid1">
    <div class="card">
      <img src="../images/invite/yvon_robert.png" alt="Yvon_robert" style="width:100%">
      <div class="container" id="conta1">
        <h2>Yvon Robert </h2>
        <p class="title" id="tit1">Maire de Rouen</p>
        <p>Yvon Robert, né le 10 novembre 1949 à Fort-de-France (Martinique), est un homme politique français, membre
        du Parti socialiste et maire de Rouen à deux reprises, entre 1995 et 2001 et depuis 2012. </p>
        <p>Yvon.R@rouen.fr</p>
        <p><button type="button" class="btn btn-primary" id="butt1" data-toggle="modal" data-target="#myModal1">
            Infos
</button></p>
      </div>
    </div>
  </div>

  <!-- 2eme personne -->
  <div class="column" id="idid2">
    <div class="card">
      <img src="../images/invite/christophe_prazuck.jpg" alt="Yvon_robert" style="width:100%">
      <div class="container" id="conta2">
        <h2>Christophe Prazuck </h2>
        <p class="title" id="tit2">Amiral</p>
        <p>Christophe Prazuck, né le 11 octobre 1960 à Oran, est un officier de marine français. 
        Promu au grade d'amiral à compter du 13 juillet 2016, il devient chef d'état-major de la Marine à cette date. </p>
        <p>prazuck.C@marine.fr </p>
        <p><button type="button" class="btn btn-primary" id="butt2" data-toggle="modal" data-target="#myModal2">
            Infos
</button></p>
      </div>
    </div>
  </div>

  <!-- 3eme personne -->
  <div class="column" id="idid3">
    <div class="card">
      <img src="../images/invite/marine_caron.jpg" alt="Yvon_robert" style="width:100%">
      <div class="container" id="conta3">
        <h2>Marine Caron </h2>
        <p class="title" id="tit3">Conseillère départementale SeineMaritime</p>
        <p>Conseillère départementale déléguée</p>
        <p> - Fondatrice de #OMILIA<p>
        <p> - Réserviste citoyenne de l'Armée de terre</p>

        <p>Marine@echos-jeunesse.com  </p>
        <p><button type="button" class="btn btn-primary" id="butt3" data-toggle="modal" data-target="#myModal3">
            Infos
</button></p>
      </div>
    </div>
  </div>

</div>

<!-- modal 1ere personne-->
<div class="modal" id="myModal1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Plus d’info :</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
      <p>Formé à l'École nationale d'administration, il travaille au sein de plusieurs cabinets ministériels durant
       la présidence de François Mitterrand, avant d'entamer une carrière politique locale dans le sillage du socialiste
       Laurent Fabius, dont il est l'un des proches.</p>
      <p>Élu maire de Rouen en 1995, il met un terme à la longue gestion centriste de la ville et mène à bien plusieurs 
      projets initiés par ses prédécesseurs mais, six ans plus tard, est battu par Pierre Albertini, candidat de l'UDF. 
      Il est, entre 2004 et 2011, conseiller général de la Seine-Maritime. </p>
      <p>Devenu premier adjoint au maire de Rouen après la large victoire de la gauche aux élections municipales de 2008,
      il remplace Valérie Fourneyron, nommée au gouvernement, comme maire de la capitale régionale en 2012. Deux ans plus tard, 
      il conserve sa fonction à l'issue du scrutin municipal. </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- modal 2eme personne-->
<div class="modal" id="myModal2">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Plus d’info :</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
      <p>Fils du contre-amiral Stéphane Prazuck1, Christophe Prazuck entre à l'École navale en septembre 1979. À l'issue de l'école
       d'application sur la Jeanne d'Arc, il est affecté de 1982 à 1984 sur le patrouilleur Altaïr puis sur le bâtiment amphibie Champlain,
        basés à La Réunion. Il reste dans la zone de l'océan Indien pour son affectation suivante en tant qu'officier en second du patrouilleur 
        Épée à Mayotte2. </p>
      <p>De 1984 à 1989, il rejoint les forces sous-marines, d'abord sur le Ouessant, puis sur la Doris comme officier opérations ; il est ensuite 
      officier de marque « armes » à la commission d’études pratiques des sous-marins2.De 1989 à 1991, il est étudiant à la United States Naval 
      Postgraduate School de Monterey en Californie où il obtient un doctorat (Ph.D.) en océanographie physique2. À l'issue de cette formation, 
      il dirige la cellule d’environnement de la marine de Toulouse2. </p>
      <p>En 2000, il rejoint le Service d'informations et de relations publiques des armées - Marine (SIRPA - Marine) où il exerce d'abord la 
      fonction de second, avant d'en prendre la direction en 2001 pour une période de trois ans. En 2004, il est placé à la tête du département 
      Médias de la Délégation à l'information et à la communication de la Défense (DICoD) puis, en 2006, il devient le conseiller communication 
      du chef d’état-major des armées2. </p>
      <p>Le 1er août 2009, il est nommé au grade de contre-amiral et prend, en août 2010, le commandement de la Force maritime des fusiliers 
      marins et commandos (FORFUSCO) à Lorient2. </p>
      <p>Promu vice-amiral d’escadre le 1er septembre 2012, il devient directeur du personnel militaire de la marine et sous-chef d’état-major -
       Ressources humaines - de l’état-major de la Marine2. </p>
       <p>Par décision du Conseil des ministres du 6 juillet 2016, il est nommé chef d'état-major de la Marine et élevé aux rang et appellation
        d’amiral, à compter du 13 juillet suivant3 en remplacement de l'Amiral Bernard Rogel promu Chef d'État-Major du Président de la 
        République. </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- modal 3eme personne-->
<div class="modal" id="myModal3">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Plus d’info :</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
      <p>Administration publique IES Normandie Ecole de Commerce, OMILIA Formation-Conseil-Communication, 
      <p>Délégation Militaire Départementale de la Seine-Maritime.</p>
      <p>Conseil Régional de Normandie, </p>
      <p>Elections Régionales 2015, </p>
      <p>Assemblée nationale – Député</p>
      <p>Université Panthéon Assas (Paris II)</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- FOOTER -->
<?php
include 'footer.inc.php';
?>


 