<!-- INCLUDE -->
<?php
$titre_page="Info pratique";
include 'header.inc.php';
include 'head.inc.php';
?>
 <div class="container">

  <p><br/></p>
  <!-- Texte des info pratique -->
  <h1>Info pratique :</h1>
    <h2>Préparez votre séjour </h2> 
      <p> A l'attention des personnes à mobilité réduite, tout sera mis en place pour faciliter votre venue.</p>
      <p> Profitez de votre venue à l’Armada pour visiter la Normandie : découvrez notre patrimoine culturel, l'histoire et le charme de notre région, la gastronomie locale...</p>
      <p> Réservez votre chambre</p>
      <p>Organisez vos visites</p>
      <p> <br /> </p>

    <h2>Comment venir ?</h2>
      <p>« Tous les océans mènent à Rouen », mais vous pouvez aussi venir en voiture, en train ou en avion : retrouvez toutes les informations pour préparer votre venue.
      L'Armada 2019 prend l'initiative de réduire l'impact sur l'environnement de plus de 8 millions de visiteurs lors de l'évènement en offrant des solutions économiques, simples et "futées".</p>
      <p> <br /> </p>

      <p><b>En voiture</b></p>
      <p>Rouen est reliée à Paris par l'autoroute A13 (130km), soit un peu plus d'une heure en voiture.
      La ville est rattachée à Caen, à la Bretagne, au Pays de la Loire et à la nouvelle Aquitaine par la même autoroute.
      Au nord, Rouen est en relation directe avec les Hauts de France et l'Angleterre par l'autoroute A28, ainsi que le tunnel sous la Manche.
      Pour plus d’informations : Les autoroutes de Normandie (SAPN)</p>
      <p>Pensez au covoiturage ! Pour ceux qui habitent à plus de 30km de l'évènement, cette option est pratique et économique. Inscrivez-vous sur www.covoiturage76.net en tant que conducteur ou passager. 
      Un autre usager vous proposera sûrement un partage des frais. C'est une manière sympathique, pratique et écologique de voyager.Où se garer en arrivant.</p>
      <p> <br /> </p>

      <p><b>En Bus/Métro</b></p>
      <p>Du 6 au 16 juin, afin de vous permettre de vous déplacer sans vague pendant l'Armada, la METROPOLE déploie un dispositif exceptionnel sur le Réseau Astuce : navettes, renforcement de l'offre de 
      transport habituelle, parkings relais, points de vente multi-sites, dispositif d'information.</p>
      <p> <br /> </p>

      <p><b>En train</b></p>
      <p>La ville de Rouen est desservie par de nombreux trains TGV, Corail et TER. (Paris, Le Havre, Marseille, Lyon, Caen, Lille, etc).</p>
      <p>Lignes TGV :
      <p> Marseille St-Charles - Rouen RD</p>
      <p>Lyon - Rouen RD</p>
      <p>Grandes lignes :
      <p> Paris St-Lazare - Rouen RD</p>
      <p> Le Havre - Rouen RD</p>
      <p>Caen - Rouen RD</p>
      <p>Dieppe - Rouen RD</p>
      <p>Lille Flandres - Rouen RD</p>
      <p><b>Lignes régionales :</b></p>
      <p>TER à destination de nombreuses communes de la région Normandie.</p>
      <p>Pour tout savoir sur les horaires des TER, la SNCF met à votre disposition un site dédié : TER Haute-Normandie
      La gare se trouve à 15 minutes à pied des quais. Vous avez la possibilité de découvrir, "en passant", les merveilles de Rouen : le Gros Horloge, la cathédrale, le vieux Rouen... 
      Vous pouvez également emprunter le métro (Gare-Rue Verte -> Théâtre des Arts).</p>
      <p>La Région Normandie mobilise ses moyens pour rendre les transports régionaux accessibles et attractifs.
      La Région Normandie, en partenariat avec la SNCF, met en place des trains spéciaux Armada chaque soir après minuit, permettant ainsi de profiter de l’ensemble des festivités.
      Durant l'évènement, des personnes seront présentes en gare de Rouen pour vous accueillir, vous renseigner et vous diriger, de 9H à 19H. Un "chalet" Armada sera installé sur le parvis de la gare.</p>
      <p> <br /> </p>
     
      <p><b>En bateau</b></p>
      <p>En provenance principalement d’Angleterre (Newhaven, Portsmouth) à destination de Caen, Le Havre ou Dieppe, vous pourrez retrouver un large choix de navettes à destination de Rouen par la route (autoroute A13).
      La circulation sur la Seine ne sera possible qu'à raison d'un passage par jour et par bateau. Pour votre sécurité, la gendarmerie vérifiera que votre embarcation possède bien un numéro de circulation ainsi que la flamme officielle aux couleurs de l’Armada 2019.</p>
      <p> <br /> </p>
      
      <p><b>En avion</b></p>
      <p>Vous venez de plus loin, optez pour l’avion. L’Aéroport de Rouen vallée de Seine et l’Aéroport de Beauvais accueillent des grandes lignes françaises et européennes à des prix compétitifs.
      Vous avez également la possibilité d'attérir sur l'un des aéroports de Paris (Orly ou Charles de Gaulle) et de prendre un train Gare Saint Lazare par la suite.</p>
      <p> <br /> </p>

      <p><b> Accès personnes à mobilité réduite</b></p>
      <p>Les personnes à mobilité réduite pourront être déposées sur certains pakings au plus près des navires. Ces parkings seront annoncés prochainement.
      Pour accéder aux quais bas, vous trouverez également un ascenseur au niveau du pont Jeanne d'Arc.</p>
      <p> <br /> </p>

    <h2>Où stationner</h2>
      <p>Découvrez les parkings disponibles pour déposer votre véhicule avant de venir profiter des festivités de l’Armada avec les navettes.</p>
      <p> Parking véhicules</p>
      <p>Parking handicapés</p>
      <p>Parking camping-cars</p>
      <p> <br /> </p>

    <h2>Sur place</h2>
      <p>Sur le site de l’Armada tout est prévu pour vous accueillir : </p>
      <p>retrouvez prochainement la localisation des points d’information, des espaces de restauration, des distributeurs automatiques,
      des commodités, des points bébé, des accès handicapés…</p>
      <p> Restaurants et Brasseries</p>
      <p>Plan détaillé des quais</p>
      <p> <br /> </p>

</div> 

<!-- FOOTER -->
<?php
include 'footer.inc.php';
?>


 