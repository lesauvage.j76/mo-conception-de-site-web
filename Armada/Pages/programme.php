<!-- INCLUDE -->
<?php
$titre_page="Programme";
include 'header.inc.php';
include 'head.inc.php';
?>

 
<!-- Texte du programme -->
<div class="container">
  <p><br/></p>
  <h1>Programme :</h1>

<h2>Mercredi 5 Juin</h2>
<p>En ouverture et en avant-première de l’Armada, la Seine s’anime … C’est la Grande pagaille. Les équipes doivent construire leur embarcation  et tentent de traverser 
la Seine sans couler à bord de leur OFNI : Objet Flottant Non Identifié.
Assistez au lancement de la 7e édition de l’Armada en découvrant cette folle traversée riche en émotion, où les équipages rivalisent d’imagination et d’originalité.
En clôture de cette journée, ne manquez pas la levée du Pont Flaubert, le soir entre 21h et minuit.</p>
<p> <br /> </p>

<h2>Jeudi 6 Juin</h2>
<p>De nouveau le Pont Flaubert se lève entre 5h et 6h du matin. Les voilà !
C’est l’arrivée des navires ! Assistez à cette remontée de la Seine des plus beaux voiliers du monde. 
Saluez les marins venus de tous les horizons qui débarquent à Rouen, sur les quais, dans la ville.
Pour les plus tardifs, la levée du pont Flaubert est également prévue le soir entre 21h et minuit.</p>
<p> <br /> </p>

<h2>Vendredi 7 Juin</h2>
<p>Soyez matinaux ! Le Pont Flaubert se lève entre 5h et 6h, une occasion de se promener sur les quais au petit matin ou alors il faut veiller tard 
car il se lève à nouveau entre 21h et minuit. Cette journée accueille le congrès du Mérite maritime, et en soirée, le magnifique feu d’artifice entre 23h et 23h30.</p>
<p> <br /> </p>

<h2>Samedi 8 Juin</h2>
<p>Grand jour pour l’Armada ! L’inauguration officielle se déroule à 11h avec l’ensemble des personnalités de la rive droite
 à la rive gauche. En début de soirée, et avant le feu d’artifice, l’Armada organise le diner des capitaines.</p>
 <p> <br /> </p>

<h2>Dimanche 9 Juin</h2>
<p>Grand moment d’émotion, fidèle à une tradition des gens de la mer en France : à 10h30, Messe des Marins. Cette messe rassemble
entre 2 000 et 5 000 personnes. L’occasion de partager avec tous les marins venus du monde entier. La messe est animée par une chorale de plus de 700 enfants. 
Et en soirée, feux d’artifice entre 23h et 23h30.</p>
<p> <br /> </p>

<h2>Lundi 10 Juin</h2>
<p>La semaine d’animations sur les quais se poursuit. Découvrez le village et les nombreuses possibilités de restauration ou d’activités,
 sur Seine ou sur les quais. Les voiliers et les navires sont ouverts à la visite. Sur les quais, les animations proposées par la Métropole
 vous attendent. A découvrir le Panorama XXL, les bars et les restaurants en bord de Seine. Les feux d’artifice vous attendent en soirée à partir de 23h.</p>
 <p> <br /> </p>

<h2>Mardi 11 Juin</h2>
<p>Depuis déjà 7 jours, Rouen est le plus grand port du monde. Les plus beaux voiliers à visiter, les bateaux promenades sur Seine vous
 permettent de les découvrir avec un autre regard, vue de Seine. Il reste encore 6 jours pour découvrir toutes les animations proposées.
Et toujours en soirée, les magnifiques feux d’artifice.</p>
<p> <br /> </p>

<h2>Mercredi 12 Juin</h2>
<p>L’Armada envahit la ville. Les quais offrent un des plus beaux spectacles au monde, celui des géants des mers et le grand défilé des équipages 
investit les rues de la ville de 14h30 à 16h30. Au rythme des musiques des fanfares, les habitants et les visiteurs découvrent les équipages dans leur tenue d’apparat.
En soirée, feux d’artifice à partir de 23h.</p>
<p> <br /> </p>

<h2>Jeudi 13 Juin</h2>
<p>Les grands voiliers et les navires sont toujours ouverts à la visite. Profitez de cette journée pour découvrir toutes les animations proposées par l’Armada
 en complément de la visite gratuite des bateaux. Village, stands, restaurants, ... A découvrir également lors de votre séjour, la ville de Rouen, sa Cathédrale,
  ses rues médiévales, ses commerces. En soirée, feux d’artifice à partir de 23h.</p>
  <p> <br /> </p>

<h2>Vendredi 14 Juin</h2>
<p>C’est le dernier week-end. Aujourd’hui, c’est le congrès des villes marraines. Les animations sur les quais battent leur plein. Que la visite des quais se déroule à l’aube, 
en journée, ou dans la nuit, la magie est la même. Feux d’artifice à partir de 23h.</p>
<p> <br /> </p>

<h2>Samedi 15 Juin</h2>
<p>A vos marques, prêts … Sportez avec les marins. Le grand footing des marins déferle sur les quais et la ville. L’occasion de partager des rencontres, des échanges, et de profiter
de leur séjour à Rouen. Les animations sur les quais, les voiliers, les navires, en ce dernier Week-end, l’Armada, c’est presque terminé….. Un dernier regard sur le plus grand port du monde
 au rythme du dernier concert de la Région Normandie et sous les lumières du dernier feu d’artifice.</p>
 <p> <br /> </p>

<h2>Dimanche 16 Juin </h2>
<p>C’est le grand jour, prenez place pour le  plus grand spectacle!
A 11h, départ de tous les voiliers pour la grande parade de Rouen à la mer, organisée par le Département de Seine Maritime. Le pont Flaubert salue ce grand départ et lève son tablier
de 10h à 16h. La patrouille de France rend également son hommage en survolant la Seine.
Tout le long de la Seine, prenez place, en familles, entre amis, et vivez ce grand moment : les équipages sont dans les voiles, les spectateurs applaudissent, agitent leurs 
drapeaux en signe d’au-revoir.
A 17h, c’est la clôture officielle de la 7e édition de l’Armada sur les quais de Rouen.</p>
<p> <br /> </p>
</div>

<!-- FOOTER -->
<?php include 'footer.inc.php'; ?>


 