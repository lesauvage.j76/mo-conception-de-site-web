<!-- INCLUDE -->
<?php
$titre_page="Sponsors et parteniare";
include 'header.inc.php';
include 'head.inc.php';
?>

<div class="container">
  <p><br/></p>
  <h2>Nous remercions nos sponsors : </h2>
  <p> <br/></p>

<h3><b>La région normandie :</b></h3> 
<a href="https://www.normandie.fr/"><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcStJw2Ru5ZkM6-z-lBv-KAK5TdRUMtyiDdRzqlwASht-R670LGb" class="img-rounded" alt="normandie" width="320" height="90"> </a>
<p>Qui finance notre édition 2019 à hauteur de 5 millions d’euros sur les 7 millions nécessaire au bon fonctionnement.</p>
<p> <br/></p>
<p> <br/></p>

<h3><b>Le département de la seine maritime :</b></h3>
<a href="https://www.seinemaritime.fr/"><img src="https://www.seinemaritime.fr/img/logofooter.png" class="img-rounded" alt="seine maritime" width="160" height="160"></a>
<p>Qui nous fournit les 2 millions d'euros restant.</p>
<p> <br/></p>
<p> <br/></p>

<h3><b>La métropole de Rouen : </b></h3>
<a href="https://www.metropole-rouen-normandie.fr/"><img src="https://metropole-rouen-normandie.jobs.net/media/2017/7/3b659cd1-3413-49ca-8be0-7c8e718043c8-1499377140976.jpg" class="img-rounded" alt="metropole rouen " width="320" height="90"></a>
<p>En échange de nous fournir notre emplacement habituel nous lui permettons d'obtenir un taux de remplissage des hôtels de 95 % et une hausse de 123 % en moyenne de fréquentation des musées durant l'Armada
57 % des visiteurs en profitent pour visiter la ville.</p>
<p> <br/></p>
<p> <br/></p>


<h3><b>La marine national :</b></h3>
<a href="https://www.defense.gouv.fr/marine"><img src="https://899unchainedblog.files.wordpress.com/2017/02/logo-etrave-marine-nationale.png?w=724" class="img-rounded" alt="marine national " width="160" height="200"></a>
<p>Qui s'occupe de communiquer notre évènement et qui attire chaque année de nouveaux responsables de bateau.</p>
<p> <br/></p>
</div>

<!-- FOOTER -->
<?php
include 'footer.inc.php';
?>


 