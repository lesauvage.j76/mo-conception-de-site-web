-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 28, 2018 at 10:09 AM
-- Server version: 10.1.26-MariaDB-0+deb9u1
-- PHP Version: 7.0.30-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bdd_7_3`
--

-- --------------------------------------------------------

--
-- Table structure for table `bateau`
--

CREATE TABLE `bateau` (
  `NOM_BATEAU` varchar(20) COLLATE armscii8_bin DEFAULT NULL,
  `MAIL_CAPITAINE` varchar(40) COLLATE armscii8_bin DEFAULT NULL,
  `PAY_CONSTRUCTION` varchar(20) COLLATE armscii8_bin DEFAULT NULL,
  `MATRICULE` varchar(20) COLLATE armscii8_bin NOT NULL,
  `DATE_FABRICATION` date DEFAULT NULL,
  `LONGUEUR` float DEFAULT NULL,
  `POIDS` float DEFAULT NULL,
  `ARMADA_PRECEDENT` varchar(100) COLLATE armscii8_bin DEFAULT NULL,
  `DESCRIPTION_BATEAU` varchar(400) COLLATE armscii8_bin DEFAULT NULL,
  `NB_CABINE` int(11) DEFAULT NULL,
  `NB_PASSAGER` int(11) DEFAULT NULL,
  `PORT_ATTACHE` varchar(100) COLLATE armscii8_bin DEFAULT NULL,
  `VITESSE_MAX` float DEFAULT NULL,
  `NB_EQUIPAGE` int(11) DEFAULT NULL,
  `CHANTIER` varchar(100) COLLATE armscii8_bin DEFAULT NULL,
  `DATE_ARRIVEE` date DEFAULT NULL,
  `DATE_DEPART` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

--
-- Dumping data for table `bateau`
--

INSERT INTO `bateau` (`NOM_BATEAU`, `MAIL_CAPITAINE`, `PAY_CONSTRUCTION`, `MATRICULE`, `DATE_FABRICATION`, `LONGUEUR`, `POIDS`, `ARMADA_PRECEDENT`, `DESCRIPTION_BATEAU`, `NB_CABINE`, `NB_PASSAGER`, `PORT_ATTACHE`, `VITESSE_MAX`, `NB_EQUIPAGE`, `CHANTIER`, `DATE_ARRIVEE`, `DATE_DEPART`) VALUES
('Arctis', 'resp@bateau2.fr', 'Finlande', 'ARCT1001', '2002-02-01', 15, 2, '1997', 'La description du Arctis', 1, 10, 'Dieppe', 32, 5, 'Paris', '2018-11-30', '2018-12-02'),
('Chardacier', 'resp@bateau1.fr', 'France', 'CHAR2014', '2014-09-14', 200, 800, '1997', 'Une description du bateau le Chardacier', 30, 500, 'Rouen', 2, 5, 'Rouen', '2018-11-29', '2018-11-30'),
('Comet', 'resp@bateau5.fr', 'Belgique', 'COM023', '1919-11-05', 25, 23, '1997', 'Une description du bateau belge le Comet', 50, 500, 'Dieppe', 12, 5, 'Dieppe', '2018-12-03', '2018-12-07'),
('Misericorde', 'resp@bateau3.fr', 'France', 'MISER0000', '2004-08-02', 2, 1, '1997', 'Une description pour la misericorde', 1, 2, 'Ile perdue', 1, 5, 'Ile perdue', '2018-12-08', '2018-12-09'),
('Opolausse', 'resp@bateau4.fr', 'Autriche', 'OPOL84', '2001-11-06', 21, 2, '1997', 'Une description pour le Opolausse', 30, 85, 'Auray', 32, 5, 'Auray', '2018-12-07', '2018-12-08'),
('Bayern', 'resp@bateau6.fr', 'Allemagne', 'SMSBA1', '1913-08-20', 180, 20000, '1997', 'Une description pour le bateau le Bayern', 600, 5000, 'Hambourg', 22, 5, 'Hambourg', '2018-11-30', '2018-12-07'),
('toto', 'toto@toto.fr', 'France', 'TOTO', '2018-11-21', 1, 1, '1997', 'toto', 1, 1, 'toto', 1, 5, 'toto', '2018-11-29', '2018-11-30');

-- --------------------------------------------------------

--
-- Table structure for table `personne`
--

CREATE TABLE `personne` (
  `ID_PERSONNE` int(11) NOT NULL,
  `NOM` varchar(20) COLLATE armscii8_bin NOT NULL,
  `PRENOM` varchar(20) COLLATE armscii8_bin NOT NULL,
  `DATE_NAISSANCE` date NOT NULL,
  `MAIL` varchar(40) COLLATE armscii8_bin NOT NULL,
  `MDP` varchar(255) COLLATE armscii8_bin NOT NULL,
  `FONCTION` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

--
-- Dumping data for table `personne`
--

INSERT INTO `personne` (`ID_PERSONNE`, `NOM`, `PRENOM`, `DATE_NAISSANCE`, `MAIL`, `MDP`, `FONCTION`) VALUES
(10, 'lesauvage', 'jeremy', '1996-11-19', 'inscrit@gmail.com', '2cb21735e85ded099a7d793d3d576eab', 0),
(11, 'responsable1', 'bateau1', '1999-01-01', 'resp@bateau1.fr', '1697918c7f9551712f531143df2f8a37', 1),
(19, 'responsable2', 'bateau2', '0001-01-01', 'resp@bateau2.fr', '1697918c7f9551712f531143df2f8a37', 1),
(14, 'responsable3', 'bateau3', '0001-01-01', 'resp@bateau3.fr', '1697918c7f9551712f531143df2f8a37', 1),
(15, 'responsable4', 'bateau4', '0001-01-01', 'resp@bateau4.fr', '1697918c7f9551712f531143df2f8a37', 1),
(16, 'responsable5', 'bateau5', '0001-01-01', 'resp@bateau5.fr', '1697918c7f9551712f531143df2f8a37', 1),
(17, 'responsable6', 'bateau6', '0001-01-01', 'resp@bateau6.fr', '1697918c7f9551712f531143df2f8a37', 1),
(18, 'responsable7', 'bateau7', '0001-01-01', 'resp@bateau7.fr', '1697918c7f9551712f531143df2f8a37', 1),
(12, 'root', 'admin', '0001-01-01', 'root@admin.com', '21232f297a57a5a743894a0e4a801fc3', 2),
(20, 'toto', 'toto', '2013-10-27', 'toto@toto.fr', 'f71dbe52628a3f83a77ab494817525c6', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bateau`
--
ALTER TABLE `bateau`
  ADD PRIMARY KEY (`MATRICULE`);

--
-- Indexes for table `personne`
--
ALTER TABLE `personne`
  ADD PRIMARY KEY (`MAIL`),
  ADD UNIQUE KEY `ID_PERSONNE` (`ID_PERSONNE`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `personne`
--
ALTER TABLE `personne`
  MODIFY `ID_PERSONNE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
