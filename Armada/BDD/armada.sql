-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 26, 2018 at 07:53 PM
-- Server version: 5.7.11
-- PHP Version: 5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `armada`
--

-- --------------------------------------------------------

--
-- Table structure for table `bateau`
--

CREATE TABLE `bateau` (
  `NOM_BATEAU` varchar(20) COLLATE armscii8_bin DEFAULT NULL,
  `MAIL_CAPITAINE` varchar(40) COLLATE armscii8_bin DEFAULT NULL,
  `PAY_CONSTRUCTION` varchar(20) COLLATE armscii8_bin DEFAULT NULL,
  `MATRICULE` varchar(20) COLLATE armscii8_bin NOT NULL,
  `DATE_FABRICATION` date DEFAULT NULL,
  `LONGUEUR` float DEFAULT NULL,
  `POIDS` float DEFAULT NULL,
  `ARMADA_PRECEDENT` varchar(100) COLLATE armscii8_bin DEFAULT NULL,
  `DESCRIPTION_BATEAU` varchar(400) COLLATE armscii8_bin DEFAULT NULL,
  `NB_CABINE` int(11) DEFAULT NULL,
  `NB_PASSAGER` int(11) DEFAULT NULL,
  `PORT_ATTACHE` varchar(100) COLLATE armscii8_bin DEFAULT NULL,
  `VITESSE_MAX` float DEFAULT NULL,
  `NB_EQUIPAGE` int(11) DEFAULT NULL,
  `CHANTIER` varchar(100) COLLATE armscii8_bin DEFAULT NULL,
  `DATE_ARRIVEE` date DEFAULT NULL,
  `DATE_DEPART` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `personne`
--

CREATE TABLE `personne` (
  `ID_PERSONNE` int(11) NOT NULL,
  `NOM` varchar(20) COLLATE armscii8_bin NOT NULL,
  `PRENOM` varchar(20) COLLATE armscii8_bin NOT NULL,
  `DATE_NAISSANCE` date NOT NULL,
  `MAIL` varchar(40) COLLATE armscii8_bin NOT NULL,
  `MDP` varchar(255) COLLATE armscii8_bin NOT NULL,
  `FONCTION` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

--
-- Dumping data for table `personne`
--

INSERT INTO `personne` (`ID_PERSONNE`, `NOM`, `PRENOM`, `DATE_NAISSANCE`, `MAIL`, `MDP`, `FONCTION`) VALUES
(10, 'lesauvage', 'jeremy', '1996-11-19', 'inscrit@gmail.com', '2cb21735e85ded099a7d793d3d576eab', 0),
(11, 'bateau', 'responsable', '1999-01-01', 'resp@bateau.fr', '1697918c7f9551712f531143df2f8a37', 1),
(13, 'responsable2', 'bateau2', '0001-01-01', 'resp@bateau2.fr', '1697918c7f9551712f531143df2f8a37', 1),
(14, 'responsable3', 'bateau3', '0001-01-01', 'resp@bateau3.fr', '1697918c7f9551712f531143df2f8a37', 1),
(15, 'responsable4', 'bateau4', '0001-01-01', 'resp@bateau4.fr', '1697918c7f9551712f531143df2f8a37', 1),
(16, 'responsable5', 'bateau5', '0001-01-01', 'resp@bateau5.fr', '1697918c7f9551712f531143df2f8a37', 1),
(17, 'responsable6', 'bateau6', '0001-01-01', 'resp@bateau6.fr', '1697918c7f9551712f531143df2f8a37', 1),
(18, 'responsable7', 'bateau7', '0001-01-01', 'resp@bateau7.fr', '1697918c7f9551712f531143df2f8a37', 1),
(12, 'root', 'admin', '0001-01-01', 'root@admin.com', '21232f297a57a5a743894a0e4a801fc3', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bateau`
--
ALTER TABLE `bateau`
  ADD PRIMARY KEY (`MATRICULE`);

--
-- Indexes for table `personne`
--
ALTER TABLE `personne`
  ADD PRIMARY KEY (`MAIL`),
  ADD UNIQUE KEY `ID_PERSONNE` (`ID_PERSONNE`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `personne`
--
ALTER TABLE `personne`
  MODIFY `ID_PERSONNE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
