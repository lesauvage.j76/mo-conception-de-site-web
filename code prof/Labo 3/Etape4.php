<?php
include 'Etape4_Fonctions.inc';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Etape 4</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <h1>Etape 4</h1>
    <p>Maintenant : la connexion.<br />
    Attention : Avant toute chose, vérifiez l'encodage des fichiers php : UTF-8 uniquement !! pas UTF-8 with BOM<br />
    Voici un formulaire, qui va envoyer les données vers le fichier Etape4_Connect.php<br />
    Ce dernier va authentifier l'utilisateur et créer un cookie d'authentification sécurisé si l'utilisateur existe.</p>
    <p>
    <form method="post" action="Etape4_Connect.php">
    <fieldset>
        <legend>Connexion</legend>
        Login : <input type="text" name="login"/><br />
        Mot de passe : <input type="password" name="password"/>
    </fieldset>
    <input type="submit" name="submit" value="Se connecter"/>
    </form>
	</p>
</body>
</html>