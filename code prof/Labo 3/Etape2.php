<!DOCTYPE html>
<html lang="en">
<head>
    <title>Etape 2</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <h1>Etape 2</h1>
    <p>Ceci est un fichier PHP avec un accès à la base de données en lecture</p>
    <p>Voici le code PHP :</p>
    <p><code>
    	<?php
		$code =
'<?php
//On se connecte avec l\'adresse ip du serveur SQL, ici c\'est 127.0.0.1, et pas localhost
//On entre les parametres dans l\'ordre : ip, compte admin, mot de passe, nom de la base
$con = mysqli_connect(\'127.0.0.1\', \'root\', \'rootroot\', \'armada\');
if (!$con) {
    echo "Connect Error: " . mysqli_connect_error();
    exit();
}
 
//On prépare la requete SQL
$sql = \'SELECT * FROM Utilisateur\';  
 
// On lance la requête (mysqli_query) et on détermine le nombre de ligne (mysqli_num_rows)
$query  = mysqli_query($con, $sql); 
$rowCount = mysqli_num_rows($query);

//on organise $query en tableau associatif  $data[\'champ\']
//en scannant chaque enregistrement récupéré
//on en profite pour gérer l\'affichage

if ($rowCount == 0) {
    echo \'<h2>Aucun utilisateur</h2>\';
    exit();
}
else {
    echo \'<h2>Tous les utilisateurs :</h2>\';
}

//boucle
while ($data = mysqli_fetch_array($query)) { 
    // on affiche les résultats 
    echo \'Nom : <strong>\'.$data[\'Nom\'].\'</strong><br />\';
    echo \'Prenom : <strong>\'.$data[\'Prenom\'].\'</strong><br /><br />\';
}  
//On ferme la connexion à la base SQL
mysqli_close($con);
?>';
		highlight_string($code);
		?>
	</code>
    </p>
    <p>Resultats :</p>
    <p>
<?php
$con = mysqli_connect('127.0.0.1', 'root', 'rootroot', 'armada');
if (!$con) {
    echo "Connect Error: " . mysqli_connect_error();
    exit();
}
$sql = 'SELECT * FROM Utilisateur'; 
$query  = mysqli_query($con, $sql); 
$rowCount = mysqli_num_rows($query);

if ($rowCount == 0) {
    echo "<h2>Aucun utilisateur</h2>";
    exit();
}
else {
    echo'<h2>Tous les utilisateurs :</h2>';
}

while ($data = mysqli_fetch_array($query)) { 
    // on affiche les résultats 
    echo 'Nom : <strong>'.$data['Nom'].'</strong><br />';
    echo 'Prenom : <strong>'.$data['Prenom'].'</strong><br /><br />';
}

mysqli_close($con);
?>
	</p>
</body>
</html>