<!DOCTYPE html>
<html lang="en">
<head>
    <title>Etape 1</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <h1>Etape 1</h1>
    <p>Ceci est un fichier PHP avec un petit test.<br />
    Pour faire un fichier PHP, prenez votre fichier HTML et renomez le PHP<br />
    Le code PHP sera inséré dans l'HTML, avec les balises <?php $code ='<?php ?>';highlight_string($code);?></p>
    <p><a href="https://sylvie-vauthier.developpez.com/tutoriels/php/grand-debutant/">Suivez ce cours</a> pour bien démarrer en PHP.</p>
    <p>Ce fichier contient un peu de PHP en plus du code html :</p>
    <p><code>
<?php
$code =
'<?php
$age=18;
echo\'J\\\'ai \'.$age.\' ans.\';
?>';
highlight_string($code);
?>
	</code>
    </p>
    <p>Resultats :</p>
    <p>
<?php
$age=18;
echo'J\'ai '.$age.' ans.';
?>
	</p>
</body>
</html>