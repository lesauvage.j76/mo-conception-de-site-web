<?php
function Armada_Connection()
{
    $con = mysqli_connect('127.0.0.1', 'root', 'rootroot', 'armada');
    if (!$con) {
        echo "Connect Error: " . mysqli_connect_error();
        exit();
    }
    return $con;
}

function Armada_GetUtilisateurs()
{
    $con = Armada_Connection();
    $sql = 'SELECT * FROM Utilisateur'; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
    if ($rowCount == 0) {
        return null;
    }
    return $query;
}
?>