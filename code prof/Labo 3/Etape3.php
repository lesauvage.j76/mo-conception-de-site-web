<?php
include 'Etape3_Fonctions.inc';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Etape 3</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <h1>Etape 3</h1>
    <p>Ceci est le même example qu'à l'étape 2 mais en utilisant plusieurs fichiers PHP, histoire de ranger le code.<br />
    J'ai créé un fichier Etapes3_Fonctions.inc dans lequel j'ai rangé les fonctions de connection et d'accès à la base de données.<br />
    Pour l'utiliser dans mon fichier principal, je dois l'inclure au début de mon fichier : include 'Etape3_Fonctions.inc'</p>
    <p>Voici le code PHP principal :</p>
    <p><code>
    	<?php
		$code =
'<?php
include \'Etape3_Fonctions.inc\';
$users = Armada_GetUtilisateurs();
if ($users == null) {
    echo "<h2>Aucun utilisateur</h2>";
    exit();
}
else {
    echo\'<h2>Tous les utilisateurs :</h2>\';
}

while ($user = mysqli_fetch_array($users)) {
    echo \'Nom : <strong>\'.$user[\'Nom\'].\'</strong><br />\';
    echo \'Prenom : <strong>\'.$user[\'Prenom\'].\'</strong><br /><br />\';
 }
?>';
		highlight_string($code);
		?>
	</code>
    </p>
    <p>Resultats :</p>
    <p>
<?php
$users = Armada_GetUtilisateurs();
if ($users == null) {
    echo "<h2>Aucun utilisateur</h2>";
    exit();
}
else {
    echo'<h2>Tous les utilisateurs :</h2>';
}

while ($user = mysqli_fetch_array($users)) {
    echo 'Nom : <strong>'.$user['Nom'].'</strong><br />';
    echo 'Prenom : <strong>'.$user['Prenom'].'</strong><br /><br />';
 }
?>
	</p>
</body>
</html>