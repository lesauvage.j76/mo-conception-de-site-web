# ArmadaWeb

Site web pour l'armada - Esigelec

Labo 1 à 6

Labo 1 : Le dossier de conception
Le but est de créer le dossier de conception, en incluant les éléments suivant :
1. Schéma d’arborescence du site (liste des pages et liens entre les pages),
2. Maquette de la page d’accueil en version mobile (petit  écran) et fixe (écran standard de PC)
3. Description de chaque page (contenu, images, liens)
4. Une première version du diagramme de la base de données
5. Le planning prévisionnel de codage pour chaque membre du binôme.

Labo 2 : La page d'accueil
Le but est de créer la page d'accueil en HTML et CSS
1. Utiliser un éditeur de text (VS Code, Sublime Text, etc.)
2. Télécharger Bootstrap (à télécharger sur getbootstrap.com)
4. Utiliser la plateforme GitLab.com pour achiver le code et les documents

Labo 3 : PHP
Le but est de convertir la page d'accueil en PHP
1. Installation de XAMP
2. Convertir la page d'accueil en PHP
3. Créer toutes les pages en php (vide, mais fonctionnelle)

Labo 4 : Login
Le but est de connecter le site à la base de données
1. Créer les tables de login en base
2. Connecter les tables de login à la page de login (Login et Créer un compte)

Labo 5 : Les autres pages
Le but est de créer le reste du site
1. Créer les autres tables
2. Connecter les autres pages aux tables

Labo 6 : Finitions
Le but est de terminer
1. Tables, pages, css, etc.